#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "SDL.h"

////////////////////////////////////////////////////////////////////////////////
// Functions

bool StartUp();		// return false if error
void Run();
void ShutDown();
void UpdateScreen();

////////////////////////////////////////////////////////////////////////////////
// Globals

enum GP2X_INPUT
{
	GP2X_JOY_UP,
	GP2X_JOY_UP_LEFT,
	GP2X_JOY_LEFT,
	GP2X_JOY_DOWN_LEFT,
	GP2X_JOY_DOWN,
	GP2X_JOY_DOWN_RIGHT,
	GP2X_JOY_RIGHT,
	GP2X_JOY_UP_RIGHT,
	GP2X_START,
	GP2X_SELECT,
	GP2X_R,
	GP2X_L,
	GP2X_A,
	GP2X_B,
	GP2X_Y,
	GP2X_X,
	GP2X_VOL_UP,
	GP2X_VOL_DOWN,
	GP2X_JOY_PUSH,
	GP2X_INPUT_MAX
};


FILE*		g_pLogFile;
SDL_Surface*	g_pScreen;
SDL_Joystick*	g_pJoystick;
SDL_Surface*	g_pBackground;


struct SInput
{
	char*		szFileName;
	SDL_Surface*	pBitmap;
	bool			bActive;
	int			iX;
	int			iY;
};

// An ugly way to do this but its good enough for a test program.
SInput g_input[GP2X_INPUT_MAX] =
{
	{ "./gfx/joymove.bmp",	NULL,	false,	 53,  42 },	// GP2X_JOY_UP
	{ "./gfx/joymove.bmp",	NULL,	false,	  1,  42 },	// GP2X_JOY_UP_LEFT
	{ "./gfx/joymove.bmp",	NULL,	false,	  1,  94 },	// GP2X_JOY_LEFT
	{ "./gfx/joymove.bmp",	NULL,	false,	  1, 146 },	// GP2X_JOY_DOWN_LEFT
	{ "./gfx/joymove.bmp",	NULL,	false,	 53, 146 },	// GP2X_JOY_DOWN
	{ "./gfx/joymove.bmp",	NULL,	false,	105, 146 },	// GP2X_JOY_DOWN_RIGHT
	{ "./gfx/joymove.bmp",	NULL,	false,	105,  94 },	// GP2X_JOY_RIGHT
	{ "./gfx/joymove.bmp",	NULL,	false,	105,  42 },	// GP2X_JOY_UP_RIGHT
	{ "./gfx/ss.bmp",		NULL,	false,	249, 209 },	// GP2X_START
	{ "./gfx/ss.bmp",		NULL,	false,	164, 209 },	// GP2X_SELECT
	{ "./gfx/right.bmp",	NULL,	false,	265,   8 },	// GP2X_R
	{ "./gfx/left.bmp",		NULL,	false,	167,   8 },	// GP2X_L
	{ "./gfx/abxy.bmp",		NULL,	false,	167,  98 },	// GP2X_A
	{ "./gfx/abxy.bmp",		NULL,	false,	270,  98 },	// GP2X_B
	{ "./gfx/abxy.bmp",		NULL,	false,	218,  46 },	// GP2X_Y
	{ "./gfx/abxy.bmp",		NULL,	false,	218, 150 },	// GP2X_X
	{ "./gfx/vol.bmp",		NULL,	false,	 47, 209 },	// GP2X_VOL_UP
	{ "./gfx/vol.bmp",		NULL,	false,	 26, 209 },	// GP2X_VOL_DOWN
	{ "./gfx/joypush.bmp",	NULL,	false,	 53,  94 }	// GP2X_JOY_PUSH
};


////////////////////////////////////////////////////////////////////////////////
  
int main(int argc, char *argv[])
{
	if(StartUp())
		Run();

	ShutDown();

	return EXIT_SUCCESS;
}

////////////////////////////////////////////////////////////////////////////////
// return false if error

bool StartUp()
{
	SDL_Surface* pTemp;

	g_pLogFile = fopen("log.txt", "w");
	if(g_pLogFile == NULL)
	{
		return false;
	}

	fprintf(g_pLogFile, "StartUp()\n");

	if(SDL_Init(SDL_INIT_VIDEO | SDL_INIT_JOYSTICK) < 0)
	{
		fprintf(g_pLogFile, "ERROR - SDL_Init(): %s\n", SDL_GetError());
		return false;
	}

	g_pScreen = SDL_SetVideoMode(320, 240, 16, SDL_SWSURFACE);
	if(g_pScreen == NULL)
	{
		fprintf(g_pLogFile, "ERROR - SDL_SetVideoMode(): %s\n", SDL_GetError());
		return false;
	}

	// Even though the Joystick subsystem was initialized
	// and even though we'll never use this object, we still have to open it
	// so that the event subsystem will capture the joystick events.
	g_pJoystick = SDL_JoystickOpen(0);
	if(g_pJoystick == NULL)
	{
		fprintf(g_pLogFile, "ERROR - SDL_JoystickOpen(): %s\n", SDL_GetError());
		return false;
	}

	SDL_ShowCursor(SDL_DISABLE);


	pTemp = SDL_LoadBMP("./gfx/background.bmp");
	if(pTemp == NULL)
	{
		fprintf(g_pLogFile, "ERROR - %s\n", SDL_GetError());
		return false;
	}

	// Convert the bitmap surface to the same format as the display.
	// This prevents conversion on the fly and will lead to faster Blits.
	g_pBackground = SDL_DisplayFormat(pTemp);
	SDL_FreeSurface(pTemp);


	for(int i = 0; i < GP2X_INPUT_MAX; i++)
	{
		pTemp = SDL_LoadBMP(g_input[i].szFileName);
		if(pTemp == NULL)
		{
			fprintf(g_pLogFile, "ERROR - %s\n", SDL_GetError());
			return false;
		}

		// Convert the bitmap surface to the same format as the display.
		// This prevents conversion on the fly and will lead to faster Blits.
		g_input[i].pBitmap = SDL_DisplayFormat(pTemp);
		SDL_FreeSurface(pTemp);
	}

	return true;
}

////////////////////////////////////////////////////////////////////////////////

void Run()
{
	fprintf(g_pLogFile, "Run()\n");

	bool			bNeedUpdate	= true;
	SDL_Event		event;
	int			iButton;

	bool			bStart	= false;
	bool			bLeft	= false;
	bool			bRight	= false;

	while(1)
	{
		SDL_PollEvent(&event);

		if(bNeedUpdate)
		{
			UpdateScreen();
			bNeedUpdate = false;
		}

		switch(event.type)
		{
		case SDL_JOYBUTTONDOWN:
			//fprintf(g_pLogFile, "Button Down: %d\n", event.jbutton.button);

			iButton = event.jbutton.button;

			// only redraw the screen if needed
			if(!g_input[iButton].bActive)
			{
				g_input[iButton].bActive = true;
				bNeedUpdate = true;

				if(iButton == GP2X_START)
					bStart = true;
				else if(iButton == GP2X_L)
					bLeft = true;
				else if(iButton == GP2X_R)
					bRight = true;

				if(bStart && bLeft && bRight)
					return;
			}

			break;

		case SDL_JOYBUTTONUP:
			//fprintf(g_pLogFile, "Button Down: %d\n", event.jbutton.button);

			iButton = event.jbutton.button;

			g_input[iButton].bActive = false;
			bNeedUpdate = true;

			if(iButton == GP2X_START)
				bStart = false;
			else if(iButton == GP2X_L)
				bLeft = false;
			else if(iButton == GP2X_R)
				bRight = false;

			break;

		default:
			break;
		}
	}
}

////////////////////////////////////////////////////////////////////////////////

void ShutDown()
{
	if(g_pLogFile)
	{
		fprintf(g_pLogFile, "ShutDown()\n");
		fclose(g_pLogFile);
	}

	SDL_FreeSurface(g_pBackground);
	for(int i = 0; i < GP2X_INPUT_MAX; i++)
	{
		SDL_FreeSurface(g_input[i].pBitmap);
	}

	SDL_Quit();

	// Return control to the menu program.
	chdir("/usr/gp2x");
	execl("/usr/gp2x/gp2xmenu", "/usr/gp2x/gp2xmenu", NULL);
}

////////////////////////////////////////////////////////////////////////////////
// A really bad way to do this but its good enough for a test program.
// A better way would be to only redraw/reblit the dirty sections.

void UpdateScreen()
{
	//fprintf(g_pLogFile, "UpdateScreen()\n");
	SDL_Rect dest;  

	// draw background
	dest.x = 0;
	dest.y = 0;

	SDL_BlitSurface(g_pBackground, NULL, g_pScreen, &dest);

	// draw the individual input bitmaps.
	for(int i = 0; i < GP2X_INPUT_MAX; i++)
	{
		// if not active then skip
		if(!g_input[i].bActive)
			continue;

		dest.x = g_input[i].iX;
		dest.y = g_input[i].iY;

		SDL_BlitSurface(g_input[i].pBitmap, NULL, g_pScreen, &dest);
	}

	SDL_Flip(g_pScreen);
}

////////////////////////////////////////////////////////////////////////////////
