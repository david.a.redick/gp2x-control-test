# What is gp2x-control-test?

This is simple test program that will high light the joystick direction and the buttons pressed (and write the event to the log.txt file).

It is an example that shows the basics of GP2X and SDL programing. 

This program hasn't been updated since 2006-10-29.

To my knowledge there are no bugs or GP2X API changes.

# How to Use

Copy the ControlTest directory to your SD memory card.
Then run as a game from your GP2X.

To exit hold down the Left, Right and Start buttons.

# How to compile - Windows XP

Download the Unoffical GP2X Dev. Kit from: http://archive.gp2x.de/cgi-bin/cfiles.cgi?0,0,0,0,14,1362

Update the path variable in Start > Control > System > Advance > Enviroment Variables

Add the path: `C:\devkitGP2X\bin;C:\devkitGP2X\minsys\bin`

Open a shell (aka `cmd`):

	$ cd c:\projects\gp2x-control-test\Source
	$ make

NOTE: If you have a different Dev. Kit then you'll may have to edit the Makefile.

# How to compile - *nix

On your own.

If you find a dev. kit that works then please email me.

# Tested On

Tested on GP2X firmware version 2.

# Project

All code and graphics are in the public domain.

Project Home: https://gitorious.org/david-a-redick/gp2x-control-test

Maintainer: David A. Redick @gmail.com
